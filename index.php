<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Calculadora de Ponto</title>
	</head>
	<body>
		<form action="index.php" method="post">
			Digite a entrada: <input type="text" name="entrada"><br>
			Digite a ida para o almoço: <input type="text" name="ida_almoco"><br>
			Digite a volta do almoço: <input type="text" name="volta_almoco">
			<input type="submit">
		</form>

		<?php
			echo "Você entrou as: ", $_POST["entrada"], "<br>";
			echo "Você saiu para o almoço as: ", $_POST["ida_almoco"], "<br>";
			echo "Você voltou do almoço as: ", $_POST["volta_almoco"], "<br>";
		?>
	</body>
</html>